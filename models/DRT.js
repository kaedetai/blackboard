const Web3 = require('web3');
const Tx = require('ethereumjs-tx');
const provider = new Web3.providers.HttpProvider('https://ropsten.infura.io/eLnCAdN3ZvRb5rNLDUje');
const web3 = new Web3(provider);
const contractAddress = '0xef656c7fb63d964ca9a7f144d28aed9f91d2d4c5';
const abi = require('../abi/DRT.json');
const contract = new web3.eth.Contract(abi, contractAddress);
const wallet = '0xcBe3372b89C051248d67B484E5a77d5b12EACD4d';
const privateKey = '0x283033A3A475E8EA4FF61CF4F7DC5DC57DB5604428C28913F5018476EDE5A55A';
const account = web3.eth.accounts.privateKeyToAccount(privateKey);

const Mongo = require('../utils/Mongo');

var DRT = {};
DRT.mint = async (to, amount) => {
  console.log({to, amount});
  
};
DRT.transfer = async (order_id, to, amount) => {
  console.log({order_id, to, amount});
  const last_tx = await Mongo.one('tx', {}, {nonce: 1}, {_id: -1});
  const nonce = last_tx? eval(last_tx.nonce) + 1: eval(await web3.eth.getTransactionCount(wallet, 'pending'));
  const data = contract.methods.transfer(to, amount);

  //var gas = await data.estimateGas();
  //console.log('estimateGas', gas);

  //TODO: verify ERC865 object

  const tx = {
    nonce: nonce,
    chainId: 3,
    to: contractAddress,
    data: data.encodeABI(),
    value: 0,
    gasPrice: web3.utils.toHex(10.1 * 1e9),
    gas: web3.utils.toHex(250000),
  }
  console.log(tx);

  const signedTx = await account.signTransaction(tx);
  console.log('signedTx', signedTx);

  tx.time = new Date();
  tx.order_id = order_id;
  const tx_id = await Mongo.add('tx', tx);

  web3.eth.sendSignedTransaction(signedTx.rawTransaction)
  .once('transactionHash', (hash) => {
    Mongo.set('tx', tx_id, {hash, hash_time: new Date()}).then(async () => {
      console.log(tx_id, 'transactionHash', hash);
      var updated = await Mongo.set('order', order_id, {status: 'pending'});
      console.log('pending', order_id, updated);
    });
  })
  .once('receipt', (receipt) => {
    Mongo.set('tx', tx_id, {receipt, receipt_time: new Date()}).then(() => {
      console.log(tx_id, 'receipt', receipt);
    });
  })
  // .on('confirmation', (number, receipt) => {
  //   const confirmation = {
  //     number,
  //     receipt,
  //     time: new Date()
  //   }
  //   Mongo.set('tx', tx_id, confirmation).then(() => {
  //     console.log(tx_id, 'confirmation', confirmation);
  //   });
  // })
  .on('error', (error) => {
    Mongo.set('tx', tx_id, {error, error_time: new Date()}).then(() => {
      console.log(tx_id, 'error', error);
    });
  })
  .then((result) => {
    result.time = new Date();
    Mongo.set('tx', tx_id, result).then(async () => {
      console.log(tx_id, 'sendSignedTransaction', result);
      var updated = await Mongo.set('order', order_id, {status: 'success'});
      console.log('success', order_id, updated);
    });
  })
  .catch(err => console.log(err));

  return true;
}

module.exports = DRT;
