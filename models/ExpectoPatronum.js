const Web3 = require("web3");
const Mongo = require('../utils/Mongo');
const config = require('../config');
const provider = new Web3.providers.HttpProvider("https://ropsten.infura.io");
const web3 = new Web3(provider);
const contract = new web3.eth.Contract(config.abi, config.address);

var ExpectoPatronum = {};

ExpectoPatronum.transfers = async () => {
  var lastBlock = await Mongo.one('transfer', {}, {blockNumber: 1}, {blockNumber: -1});
  var fromBlock = lastBlock? (lastBlock.blockNumber + 1): config.from;
  var transfers = await contract.getPastEvents('Transfer', {fromBlock});
  for (var i in transfers) {
    var t = transfers[i];
    await Mongo.add('transfer', t);
  }
  return await Mongo.find('transfer', {}, {}, {_id: -1});
}

ExpectoPatronum.totalSupply = async () => {
  return await contract.methods.totalSupply().call();
}

ExpectoPatronum.name = async () => {
  return await contract.methods.name().call();
}

ExpectoPatronum.symbol = async () => {
  return await contract.methods.symbol().call();
}

ExpectoPatronum.decimals = async () => {
  return await contract.methods.decimals().call();
}

ExpectoPatronum.balanceOf = async (address) => {
  return await contract.methods.balanceOf(address).call();
}

ExpectoPatronum.buildMintTx = async (address, amount) => {
  return contract.methods.mint(address, amount).encodeABI();
}

ExpectoPatronum.buildBurnTx = async (amount) => {
  return contract.methods.burn(amount).encodeABI();
}

ExpectoPatronum.buildPauseTx = async () => {
  return await contract.methods.pause().encodeABI();
}

ExpectoPatronum.buildUnpauseTx = async () => {
  return await contract.methods.unpause().encodeABI();
}

ExpectoPatronum.isPaused = async () => {
  return await contract.methods.paused().call();
}

ExpectoPatronum.buildTransferTx = async (address, amount) => {
  return contract.methods.transfer(address, amount).encodeABI();
}

ExpectoPatronum.getContractAddress = function() {
  return web3.utils.toChecksumAddress(config.address);
}
module.exports = ExpectoPatronum;
