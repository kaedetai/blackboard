var data = {
	username: '',
	password: '',
	checkbox: false,
};

//data//

var vue = new Vue({
	el: '#vue',
	data: data,
	computed: {
		title: function() {
			return 'FiO BackOffice ' + ((location.host == 'mms.cto.tw')? 'MMS': 'TMS');
		}
	},
	methods: {
		login: function() {
			api('/login', data, function(result) {
				location.href = '/dashboard'
			});
		},
	}
});
