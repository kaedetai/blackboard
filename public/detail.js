var data = {
	username: 'admin',
	transactions: [],
	per: 10,
	page: 1,
};

//data//

var vue = new Vue({
	el: '#vue',
	data: data,
	computed: {
		pages: function() {
			const n = Math.ceil(data.transactions.length / data.per);
			let a = [];
			for (let i = 1; i <= n; i ++)
				a.push(i);
			return a;
		},
		from: function() {
			return (data.page - 1) * data.per + 1;
		},
		to: function() {
			return Math.min(data.transactions.length, data.page * data.per);
		},
		show: function() {
			let a = [];
			for (let i = (data.page - 1) * data.per; i < Math.min(data.transactions.length, data.page * data.per); i ++)
				a.push(data.transactions[i]);
			return a;
		},
	},
	methods: {
		format: function(ts) {
			const x = (new Date(ts)).toLocaleString('en-GB');
			return x.substr(6,4)+'-'+x.substr(3,2)+'-'+x.substr(0,2)+' '+x.substr(11,9);
		},
		prev: function() {
			if (data.page > 1) data.page --;
		},
		next: function() {
			if (data.page < Math.ceil(data.transactions.length / data.per)) data.page ++;
		},
	}
});
