var data = {
	selected: 0,
	x: 0,
	y: 0,
	zoom: 1,
	editing: 0,
	objs: [
		{ _id: 1, type: 'rect', width: 40, height: 30, x: -30, y: -65, fill: '#c00' },
		{ _id: 2, type: 'rect', width: 40, height: 30, x: 30, y: -65, fill: '#f80' },
		{ _id: 3, type: 'rect', width: 40, height: 30, x: -30, y: 65, fill: '#09c' },
		{ _id: 4, type: 'rect', width: 40, height: 30, x: 30, y: 65, fill: '#0c0' },
		{ _id: 5, type: 'rect', width: 40, height: 30, x: 0, y: 0, fill: '#fff' },
		{ _id: 6, type: 'text', x: -20, y: -30, text: '這是正常的文字', fill: '#00f' },
		{ _id: 7, type: 'path', path: '#wave1', text: '這是隨著路徑跑的文字，很酷吧', fill: '#f00' },
		{ _id: 8, type: 'path', path: '#wave2', text: '這是隨著路徑跑的文字，很酷吧', fill: '#792', dy: '50,10,20,5,40,8,20' },
	],
};

//data//

var dragging = false;
var moving = false;
var aiming = false;
var pinching = false;
var dragX = 0;
var dragY = 0;
var fromX = 0;
var fromY = 0;
var pinchZoom = 0;
var size = document.getElementById('vue').getBoundingClientRect();
var lastClick = new Date();

var vue = new Vue({
	el: '#vue',
	data: data,
	computed: {
		viewBox: function() {
			return (data.x - size.width * data.zoom / 2) + ' ' + (data.y - size.height * data.zoom / 2) + ' ' + (size.width * data.zoom) + ' ' + (size.height * data.zoom);
		},
	},
	methods: {
		aim: function(e) {
			aiming = true;
			console.log('aim', e);
		},
		edit: function(i) {
			data.editing = (data.editing == i)? 0: i;
			data.selected = 0;
		},
		select: function(_id) {
			if (!data.editing) return;
			if (moving) return;

			console.log('select', _id);
			data.selected = _id;
		},
		text: function(x, y) {
			console.log('text', x, y);

			var text = prompt('請輸入留言訊息:');
			if (!text) return;
			var fill = prompt('請輸入顏色，例如 #fff 或 #12d8f0 或 yellow :');
			var obj = { type: 'text', x: x, y: y, text: text, fill: fill };
			api('/obj', {obj}, function(obj) {
				console.log('obj', obj);
			});
		},
		image: function(x, y) {
			console.log('image', x, y);

			var src = prompt('請輸入圖片網址:');
			if (!src) return;
			var obj = { type: 'image', x: x, y: y, src: src };
			data.objs.push(obj);
			api('/obj', {obj}, function(obj) {
				console.log('obj', obj);
			});
		},
		click: function(e) {
			if (!data.editing) return;
			console.log('click', e, size);

			var x = data.x + (dragX - size.width / 2) * data.zoom;
			var y = data.y + (dragY - size.height / 2) * data.zoom;
			if (data.editing == 1)
				this.text(x, y);
			else if (data.editing == 2)
				this.image(x, y);
		},
		mousedown: function(e) {
			console.log('mousedown', e);

			data.selected = 0;
			dragging = true;
			moving = false;
			size = document.getElementById('vue').getBoundingClientRect();
			dragX = e.x;
			dragY = e.y;
			fromX = data.x;
			fromY = data.y;
		},
		mouseup: function(e) {
			console.log('mouseup', e);

			dragging = false;
			if (aiming) {
				aiming = false;
				return;
			}
			if (moving)
				moving = false;
			else
				this.click(e);
		},
		mousemove: function(e) {
			if (!dragging) return;
			console.log('mousemove', e);

			moving = true;
			data.x = fromX + (dragX - e.x) * data.zoom;
			data.y = fromY + (dragY - e.y) * data.zoom;
		},
		touchstart: function(e) {
			console.log('touchstart', e);

			data.selected = 0;
			size = document.getElementById('vue').getBoundingClientRect();
			dragging = true;
			moving = false;
			dragX = e.touches[0].pageX;
			dragY = e.touches[0].pageY;
			fromX = data.x;
			fromY = data.y;
		},
		touchend: function(e) {
			console.log('touchend', e);

			if (pinching) {
				pinching = false;
				return;
			}

			if (dragging) {
				dragging = false;
				if (aiming) {
					aiming = false;
					return;
				}
				if (moving)
					moving = false;
				else
					this.click(e);
				return;
			}
		},
		touchmove: function(e) {
			console.log('touchmove', e);

			if (pinching) {
				var p0 = e.touches[0];
				var p1 = e.touches[1];
				data.zoom = pinchZoom / Math.sqrt(Math.pow(p0.pageX - p1.pageX, 2) + Math.pow(p0.pageY - p1.pageY, 2));
				return;
			}
			if (e.touches.length > 1) {
				dragging = false;
				moving = false;
				pinching = true;
				var p0 = e.touches[0];
				var p1 = e.touches[1];
				pinchZoom = data.zoom * Math.sqrt(Math.pow(p0.pageX - p1.pageX, 2) + Math.pow(p0.pageY - p1.pageY, 2));
				return;
			}
			if (dragging) {
				moving = true;
				data.x = fromX + (dragX - e.changedTouches[0].pageX) * data.zoom;
				data.y = fromY + (dragY - e.changedTouches[0].pageY) * data.zoom;
				return;
			}
		},
		wheel: function(e) {
			if (e.deltaY > 0)
				data.zoom *= 1.1;
			else
				data.zoom /= 1.1;
			console.log(data.x, data.y, data.zoom);
		},
	},
});

document.onkeydown = function(e) {
	if (!data.selected || e.keyCode != 8) return;

	console.log('delete', data.selected);
	for (var i in data.objs) {
		if (data.objs[i]._id == data.selected) {
			data.objs[i].type = 'deleted';
			data.selected = 0;
		}
	}
};

var io = io();
io.emit('hi', 'hihi');

io.on('obj', function(obj) {
	console.log('obj', obj);
	data.objs.push(obj);
});
