var data = {
	username: 'admin',
  lct2fio: [{"_id":1,"type":"lct2fio","account":"0939630727","lct_name":"Travio","amount":"1","transactionId":"759d482589a05479ea86ba86309d8e0802cf33943a385dee16d3e8c8bcab4ab3"}],
  fio2lct: [{"_id":2,"type":"fio2lct","account":"0939630727","lct_name":"Travio","amount":"1","transactionId":"d3aad2e0030e539713ff6c7e1f9a581d95446e126349b525a788b5b7cc780b38"}],
  transfer: [{"_id":5,"from_account_id":"1","to_account_id":"2","merchant_id":"1","amount":"1","transactionId":"0cfc1eed1e50ab7d9cca8e220d0f773574089866165321ee1e9965ac0b7c4528"}],
  accounts: {"1":{"account":"0939630727"},"2":{"account":"0933222111"},"3":{"account":"0939630728"},"4":{"account":"0933000111"},"5":{"account":"09555555"}},
  merchants: {"1":{"name":"Travio"},"2":{"name":"Gabro"}},
};

//data//

var vue = new Vue({
	el: '#vue',
	data: data,
	methods: {
		login: function() {
			api('/print', data, function(result) {
				alert(result);
			});
		},
    getAccount: function(id) {
      if (!this.accounts[id]) return '';
      return this.accounts[id].account;
    },
    getMerchant: function(id) {
      if (!this.accounts[id]) return '';
      return this.merchants[id].name;
    },
	}
});
