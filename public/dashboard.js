var data = {
  username: 'admin',
  type: 'admin',
	transactions: [
		{
			lct: 'FiO',
			last: 123,
			amount: 100,
		},
		{
			lct: 'Travio',
			last: 321,
			amount: 200,
		},
		{
			lct: 'ExR',
			last: 333,
			amount: 300,
		},
	],
};

//data//

var vue = new Vue({
	el: '#vue',
	data: data,
	methods: {
		login: function() {
			api('/detail', data, function(result) {
				alert(result);
			});
		},
	}
});



function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 15,
		center: {lat: 22.277551, lng: 114.172988},
		mapTypeId: 'roadmap'
	});

	var icons = {
		Gabro: {
			name: 'Gabro',
			icon: '/img/Gabro.png'
		},
		HKML: {
			name: 'HKML',
			icon: '/img/HKML.png'
		},
		'E-Points': {
			name: 'E-Points',
			icon: '/img/E-Points.png'
		},
		'Travio':{
			name:'Travio',
			icon:'/img/Travio.png'
		}
	};
  
	function addMarker(point) {
	  var marker = new google.maps.Marker({
			position: point.position,
			icon: icons[point.type].icon,
			map: map,
			info: 'Point: ' + point.type + '\nProduct: ' + point.product + '\nPrice: ' + point.price + '\nAmount: ' + point.amount,
	  });
  
	  marker.addListener('click', function() {
		alert(marker.info);
	  });
	}
  var users = [
		{
			_id: 0,
			name: 'Amy',
			age: 40,
			height: 162,
			gender:0
		},
		{
			_id: 1,
			name: 'mary',
			age: 23,
			height: 158,
			gender:0
		},
		{
			_id: 2,
			name: 'jake',
			age: 45,
			height: 172,
			gender:1
		},
		{
			_id: 3,
			name: 'alison',
			age: 30,
			height: 165,
			gender:0
		},
		{
			_id: 4,
			name: 'Amanda',
			age: 35,
			height: 166,
			gender:0
		},
		{
			_id: 5,
			name: 'mark',
			age: 28,
			height: 178,
			gender:1
		},
		{
			_id: 6,
			name: 'andy',
			age: 55,
			height: 170,
			gender:1
		},
		{
			_id: 7,
			name: 'bill',
			age: 25,
			height: 177,
			gender:1
		},
		{
			_id: 8,
			name: 'jane',
			age: 45,
			height: 155,
			gender:0
		},
		{
			_id: 9,
			name: 'carlin',
			age: 33,
			height: 163,
			gender:0
		}
	];
	var points = [
	  {
		user_id:0,
		transaction_id:0,
		product:'bread',
		amount:2,
		price:20,
		position: {lat: 22.279804, lng: 114.171969},
		type: 'Gabro'
	  },
	  {
		user_id:2,
		transaction_id:1,
		product:'Durian',
		amount:1,
		price:150,
		position: {lat: 22.279714, lng: 114.171862},
		type: 'Gabro'
	  },
	  {
		user_id:4,
		transaction_id:2,
		product:'cigarette',
		amount:2,
		price:100,
		position: {lat: 22.279814, lng: 114.172079},
		type: 'Gabro'
		},
		{
		user_id:5,
		transaction_id:3,
		product:'jenny bakery',
		amount:5,
		price:80,
		position: {lat: 22.533644, lng: 113.980830},
		type: 'Gabro'
		},
		{
		user_id:8,
		transaction_id:4,
		product:'egg roll',
		amount:3,
		price:30,
		position: {lat: 22.297334, lng: 114.175338},
		type: 'Gabro'
		},
		{
		user_id:4,
		transaction_id:5,
		product:'Fish ball noodles',
		amount:2,
		price:100,
		position: {lat: 22.279813, lng: 114.172074},
		type: 'Gabro'
		},
		{
		user_id:5,
		transaction_id:6,
		product:'egg custard pudding',
		amount:5,
		price:80,
		position: {lat: 22.304787, lng: 114.170524},
		type: 'Gabro'
		},
		{
		user_id:8,
		transaction_id:7,
		product:'Dim Dim',
		amount:3,
		price:500,
		position: {lat: 22.307519, lng: 114.166137},
		type: 'Gabro'
		},
	  {
		user_id:0,
		transaction_id:8,
		product:'Rhytidectomy',
		amount:1,
		price:2000,
		position: {lat: 22.275575, lng: 114.172613},
		type: 'HKML',
	  },
	  {
		user_id:1,
		transaction_id:9,
		product:'Acne Care',
		amount:1,
		price:500,
		position: {lat: 22.275675, lng: 114.172713},
		type: 'HKML'
	  },
	  {
		user_id:2,
		transaction_id:10,
		product:'Asian blepharoplasty',
		amount:1,
		price:3000,
		position: {lat: 22.275775, lng: 114.172413},
		type: 'HKML'
		},
		{
		user_id:3,
		transaction_id:11,
		product:'Suction-Assisted Lipectomy',
		amount:1,
		price:3500,
		position: {lat: 22.297969, lng: 114.172861},
		type: 'HKML'
		},
		{
		user_id:6,
		transaction_id:12,
		product:'anti-ageing therapies',
		amount:1,
		price:1000,
		position: {lat: 22.284629, lng: 114.155400},
		type: 'HKML'
		},
		{
		user_id:7,
		transaction_id:13,
		product:'Brow Lift',
		amount:2,
		price:200,
		position: {lat: 22.271887, lng:  114.131282},
		type: 'HKML'
		},
		{
		user_id:9,
		transaction_id:14,
		product:'Breast Lift',
		amount:2,
		price:1500,
		position: {lat: 22.271882, lng:  114.124973},
		type: 'HKML'
		},
		{
		user_id:3,
		transaction_id:15,
		product:'Acne Care',
		amount:2,
		price:500,
		position: {lat: 22.279288, lng:  114.188386},
		type: 'HKML'
		},
		{
		user_id:4,
		transaction_id:16,
		product:'Rhytidectomy',
		amount:2,
		price:2000,
		position: {lat: 22.282354, lng:  114.182242},
		type: 'HKML'
		},
		{
		user_id:5,
		transaction_id:17,
		product:'Brow Lift',
		amount:2,
		price:200,
		position: {lat: 22.319403, lng:  114.169942},
		type: 'HKML'
		},
		{
		user_id:6,
		transaction_id:18,
		product:'Brow Lift',
		amount:1,
		price:200,
		position: {lat: 22.373339, lng:  114.177138},
		type: 'HKML'
		},
		{
		user_id:6,
		transaction_id:19,
		product:'anti-ageing therapies',
		amount:2,
		price:250,
		position: {lat: 22.306956, lng:  114.171371},
		type: 'HKML'
		},
		{
		user_id:8,
		transaction_id:20,
		product:'anti-ageing therapies',
		amount:4,
		price:200,
		position: {lat: 22.442492, lng:  114.028878},
		type: 'HKML'
		},
		{
		user_id:2,
		transaction_id:21,
		product:'Facelift',
		amount:2,
		price:300,
		position: {lat: 22.282259, lng:  113.939256},
		type: 'HKML'
		},
		{
		user_id:5,
		transaction_id:22,
		product:'Facelift',
		amount:330,
		price:250,
		position: {lat: 22.525562, lng:  113.996176},
		type: 'HKML'
		},
	  {
		user_id:0,
		transaction_id:23,
		product:'carshare',
		amount:2,
		price:2000,
		position: {lat: 22.278424, lng: 114.176679},
		type: 'E-Points'
	  },
	  {
		user_id:3,
		transaction_id:24,
		product:'KAYAK',
		amount:3,
		price:4500,
		position: {lat: 22.278624, lng: 114.176579},
		type: 'E-Points'
	  },
	  {
		user_id:7,
		transaction_id:25,
		product:'gobee bike',
		amount:10,
		price:100,
		position: {lat: 22.278924, lng: 114.176889},
		type: 'E-Points'
		},
		{
		user_id:2,
		transaction_id:26,
		product:'Wan Chai Ferry Pier Ticket',
		amount:2,
		price:20,
		position: {lat: 22.283712, lng: 114.176193},
		type: 'E-Points'
		},
		{
		user_id:5,
		transaction_id:27,
		product:'Dragon Pearl Cruise ticket',
		amount:3,
		price:450,
		position: {lat: 22.278624, lng: 114.176579},
		type: 'E-Points'
		},
		{
		user_id:9,
		transaction_id:28,
		product:'Peak Tram Ticket',
		amount:10,
		price:100,
		position: {lat: 22.276358, lng: 114.157915},
		type: 'E-Points'
		},
		{
		user_id:5,
		transaction_id:29,
		product:'Ocen Park Peak Tram Ticket',
		amount:3,
		price:50,
		position: {lat: 22.246631, lng: 114.176495},
		type: 'E-Points'
		},
		{
		user_id:3,
		transaction_id:30,
		product:'Ngong ping 360 Peak Tram Ticket',
		amount:10,
		price:100,
		position: {lat: 22.256532, lng: 113.901487},
		type: 'E-Points'
		},
		{
		user_id:6,
		transaction_id:31,
		product:'Hong Kong Heritage Museum Ticket',
		amount:10,
		price:1000,
		position: {lat: 22.377394, lng: 114.185377},
		type: 'E-Points'
		},
		{
		user_id:0,
		transaction_id:32,
		product:'Phillip Mini Crossbody',
		amount:1,
		price:4400,
		position:{lat:22.305850,lng:114.165039},
		type:'Travio'
		},
		{
		user_id:4,
		transaction_id:33,
		product:'Roast pork',
		amount:1,
		price:233,
		position:{lat:22.311601,lng:114.170132},
		type:'Travio'
			},
		{
		user_id:4,
		transaction_id:34,
		product:'Disney Ticket',
		amount:2,
		price:1500,
		position:{lat:22.313105,lng:114.041251},
		type:'Travio'
		},
		{
		user_id:7,
		transaction_id:35,
		product:'Chef Mickey',
		amount:1,
		price:1000,
		position:{lat:22.309602,lng:114.037385},
		type:'Travio'
		},
		{
		user_id:6,
		transaction_id:36,
		product:'street food',
		amount:2,
		price:80,
		position:{lat:22.249088,lng:114.176153},
		type:'Travio'
		},
		{
		user_id:6,
		transaction_id:37,
		product:'Ocean Park ticket',
		amount:3,
		price:1500,
		position:{lat:22.246883,lng:114.174265},
		type:'Travio'
		},
		{
		user_id:8,
		transaction_id:38,
		product:'Dim sum',
		amount:1,
		price:200,
		position:{lat:22.249453,lng:114.177013},
		type:'Travio'
		},
		{
		user_id:2,
		transaction_id:39,
		product:'Cartier watch',
		amount:1,
		price:675,
		position:{lat:22.299148,lng:114.166427},
		type:'Travio'
		},
	];
  
  if (vue.type != 'admin') {
    points = points.filter(function(x) {
      return x.type == vue.type;
    });
  }
	for (var i in points) {
	  addMarker(points[i]);
	}
  
}

function hide() {
  var a = $('a[href="http://www.amcharts.com"]');
	$('image[x]').parent().parent().hide();
	a.hide();
  setTimeout(hide, 500);
}

function draw(points) {
  if (!points.length) {
    $('#section-4').hide();
    return;
  }

  var options = {
		"type": "serial",
		"addClassNames": true,
		"theme": "light",
		"autoMargins": true,
		"marginLeft": 0,
		"marginRight": 0,
		"marginTop": 25,
		"marginBottom": 0,
		"balloon": {
			"adjustBorderColor": false,
			"horizontalPadding": 10,
			"verticalPadding": 8,
			"color": "#ffffff"
		},
			"legend": {
					"equalWidths": false,
					"useGraphSettings": true,
					"valueAlign": "left",
					"valueWidth": 120
			},
		"dataProvider": points,
		"valueAxes": [ {
			"id": "money",
			"title": "Total Amount (K)",
			"axisAlpha": 0,
			"position": "left"
		}, {
			"id": "people",
			"title": "Transactions",
			"axisAlpha": 0,
			"position": "right",
			"labelFunction": function(value) {
				return value + " Transactions";
			}
		} ],
		"startDuration": 1,
		"graphs": [
			{
				"valueAxis": "people",
				"alphaField": "alpha",
				"balloonText": "<span style='font-size:12px;'>[[date]][[title]]:<br><span style='font-size:20px;'>[[value]] Transactions</span> [[additional]]</span>",
				"fillAlphas": 1,
				"title": "Transactions",
				"type": "column",
				"valueField": "count",
				"dashLengthField": "dashLengthColumn"
			},
			{
				"valueAxis": "money",
				"balloonText": "<span style='font-size:12px;'>[[date]] [[title]]:<br><span style='font-size:20px;'>[[value]]K</span> [[additional]]</span>",
				"bullet": "round",
				"lineThickness": 3,
				"bulletSize": 7,
				"bulletBorderAlpha": 1,
				"bulletColor": "#FFFFFF",
				"useLineColorForBulletBorder": true,
				"bulletBorderThickness": 3,
				"fillAlphas": 0,
				"lineAlpha": 1,
				"title": "Gabro",
				"valueField": "p1",
				"dashLengthField": "dashLengthLine"
			},
			{
				"valueAxis": "money",
				"balloonText": "<span style='font-size:12px;'>[[date]] [[title]]:<br><span style='font-size:20px;'>[[value]]K</span> [[additional]]</span>",
				"bullet": "round",
				"lineThickness": 3,
				"bulletSize": 7,
				"bulletBorderAlpha": 1,
				"bulletColor": "#FFFFFF",
				"useLineColorForBulletBorder": true,
				"bulletBorderThickness": 3,
				"fillAlphas": 0,
				"lineAlpha": 1,
				"title": "Travio",
				"valueField": "p2",
				"dashLengthField": "dashLengthLine"
			},
			{
				"valueAxis": "money",
				"balloonText": "<span style='font-size:12px;'>[[date]] [[title]]:<br><span style='font-size:20px;'>[[value]]K</span> [[additional]]</span>",
				"bullet": "round",
				"lineThickness": 3,
				"bulletSize": 7,
				"bulletBorderAlpha": 1,
				"bulletColor": "#FFFFFF",
				"useLineColorForBulletBorder": true,
				"bulletBorderThickness": 3,
				"fillAlphas": 0,
				"lineAlpha": 1,
				"title": "HKML",
				"valueField": "p3",
				"dashLengthField": "dashLengthLine"
			},
			{
				"valueAxis": "money",
				"balloonText": "<span style='font-size:12px;'>[[date]] [[title]]:<br><span style='font-size:20px;'>[[value]]K</span> [[additional]]</span>",
				"bullet": "round",
				"lineThickness": 3,
				"bulletSize": 7,
				"bulletBorderAlpha": 1,
				"bulletColor": "#FFFFFF",
				"useLineColorForBulletBorder": true,
				"bulletBorderThickness": 3,
				"fillAlphas": 0,
				"lineAlpha": 1,
				"title": "E-Points",
				"valueField": "p4",
				"dashLengthField": "dashLengthLine"
			},
		],
		"dataDateFormat": "MM/DD",
		"categoryField": "date",
		"categoryAxis": {
			"dateFormats": [{"period":"DD","format":"MM/DD"},{"period":"WW","format":"MM/DD"},{"period":"MM","format":"MMM"},{"period":"YYYY","format":"YYYY"}],
			"parseDates": true,
			"minPeriod": "DD",
			"gridPosition": "start",
			"axisAlpha": 0,
			"tickLength": 0
		},
  };

  if (vue.type != 'admin') {
    options.graphs = options.graphs.filter(function(x) {
      return x.valueAxis != 'money' || x.title == vue.type;
    });
  }

  AmCharts.makeChart("chartdiv", options);

	hide();
}

var points = [
	{"date":"03/01","p1":0,"p2":0,"p3":0,"p4":0,"taiex":9930.74,"count":0,"gain":-1.2},
	{"date":"03/02","p1":98.8,"p2":0,"p3":1.2,"p4":0,"taiex":9902.98,"count":14,"gain":-1.2},
	{"date":"03/03","p1":87.3,"p2":46.5,"p3":0.7,"p4":0,"taiex":9876.77,"count":18,"gain":-1.2},
	{"date":"03/04","p1":119.6,"p2":20.9,"p3":2.2,"p4":6.1,"taiex":9876.45,"count":15,"gain":-1.2},
	{"date":"03/05","p1":84.9,"p2":35.15,"p3":4.5,"p4":6.3,"taiex":9856.25,"count":9,"gain":0.9},
	{"date":"03/06","p1":95.9,"p2":55.15,"p3":6.4,"p4":17,"taiex":9848.15,"count":10,"gain":0.9},
	{"date":"03/07","p1":100.5,"p2":65.15,"p3":17.6,"p4":30.6,"taiex":9811.52,"count":20,"gain":0.9},
	{"date":"03/08","p1":107.4,"p2":35.15,"p3":42.6,"p4":26.4,"taiex":9949.48,"count":22,"gain":0.9},
];
draw(points);

