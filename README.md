# blackboard - a visible blockchain universe
## Installation
install node.js 10 and pm2
```
sudo npm install -g n pm2
sudo n 10
```
install modules
```
npm install
```
start service and watch logs
```
npm start
pm2 l
pm2 logs
```
## How to use it
connect to http://localhost:8080/index.html
## Files & Folders
| Name      | Description                                      |
|-----------|--------------------------------------------------|
| abi/      | where the ABI files go                           |
| api.js    | res.data() for Vue.js data and res.ok() for API  |
| app.js    | web server config, modules and middlewares       |
| config.js | the configurations                               |
| models/   | models that interact with DB, BlockChain, etc... |
| public/   | document root for web service                    |
| routes/   | the data APIs and APIs for Apps                  |
| sio.js    | socket.io for bi-directional messaging           |
| tmp       | the main program                                 |
| utils/    | tools                                            |
## How it works
1. public/index.html calls /index API in routes/index.js
2. the get data from blockchain
3. compose data with public/index.js using res.data() in api.js
4. Vue.js render the page

