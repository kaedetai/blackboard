var request = require('request-promise');

var Post = (uri, qs) => request({method: 'POST', uri: uri, form: qs});
Post.json = (uri, qs) => request({method: 'POST', uri: uri, body: qs, json: true});

module.exports = Post;
