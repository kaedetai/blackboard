var crypto = require('crypto');

var api_pw = '1e79ce6e991f50c8abaa8f2d63d1da71';
var api_iv = 'c94ca0db0ea8a740';

var AES = () => {};

AES.encAPI = text => {
  if (!text) return null;
  var enc = crypto.createCipheriv('aes-256-cbc', api_pw, api_iv);
  return enc.update(text, 'utf8', 'hex') + enc.final('hex');
};

AES.decAPI = cipher => {
  if (!cipher) return null;
  var dec = crypto.createDecipheriv('aes-256-cbc', api_pw, api_iv);
  return dec.update(cipher, 'hex', 'utf8') + dec.final('utf8');
};

module.exports = AES;
