// Imports
var api = require('../api')('/');
const Web3 = require('web3');

// Include models
var token = require('../models/ExpectoPatronum')
// var token = require('../models/DRT')

// Define Constants
const provider = new Web3.providers.HttpProvider('https://ropsten.infura.io/eLnCAdN3ZvRb5rNLDUje');
const privkey = '0x283033A3A475E8EA4FF61CF4F7DC5DC57DB5604428C28913F5018476EDE5A55A'

/**
 * Get Balance from Address
 */
api.post('/balance', async (req, res) => {
  var address = req.body.address;
  if (!address) return res.err(1, 'Params Missing or of Wrong-type');

  var totalSupply = token.totalSupply();
  var balance = token.balanceOf(address);
  res.ok({balance: await balance, totalSupply: await totalSupply});
});

/**
 * Mint ERC-20 Token into Treasury
 */
api.post('/mint', async (req, res) => {
  var address = req.body.address;
  var amount = parseInt(req.body.amount);
  if (!address || !amount) return res.err(1, 'Params Missing or of Wrong-type');

  // Perform Minting
  const web3 = new Web3(provider);
  address = web3.utils.toChecksumAddress(address);
  web3.eth.getAccounts(async function(err, accounts) {
    const account = web3.eth.accounts.privateKeyToAccount(privkey);
    var tNonce = eval(web3.eth.getTransactionCount(account.address, 'pending'));
    var tTmp = eval(web3.eth.getTransactionCount(account.address));
    var [nonce, tmp] = [await tNonce, await tTmp];
    if (nonce !== tmp) return res.err(2, 'This account has pending transaction! Please wait.');
    const contract_address = token.getContractAddress();
    console.log('ContractAddress: ', contract_address);
    if (!contract_address) return res.err(3, 'Contract Address null', contract_address);
    const raw = await token.buildMintTx(address, amount);
    const tx = {
      nonce, chainId: 3, value: 0,
      to: contract_address,
      data: raw,
      gasPrice: web3.utils.toHex(10.1 * 1e9),
      gas: web3.utils.toHex(250000),
    }
    const signedTx = await account.signTransaction(tx);
    tx.time = new Date();
    web3.eth.sendSignedTransaction(signedTx.rawTransaction)
      .then(txn_hash => {
        res.ok({txn_hash});
      }).catch(err => {
        res.err(4, 'Transaction error', err);
      }); // sendSignedTransaction
  }).catch(err => {
    res.err(5, 'Account error', err)
  }); // getAccounts
});

/**
 * Burn ERC-20 Token from Treasury
 */
api.post('/burn', async (req, res) => {
  var amount = parseInt(req.body.amount);
  if (!amount) return res.err(1, 'Params Missing or of Wrong-type');

  // Perform Burning
  const web3 = new Web3(provider);
  web3.eth.getAccounts(async function(err, accounts) {
    const account = web3.eth.accounts.privateKeyToAccount(privkey);
    var tNonce = eval(web3.eth.getTransactionCount(account.address, 'pending'));
    var tTmp = eval(web3.eth.getTransactionCount(account.address));
    var [nonce, tmp] = [await tNonce, await tTmp];
    if (nonce !== tmp) return res.err(2, 'This account has pending transaction! Please wait.');
    const contract_address = token.getContractAddress();
    console.log('ContractAddress: ', contract_address);
    if (!contract_address) return res.err(3, 'Contract Address null', contract_address);
    const raw = await token.buildBurnTx(amount);
    const tx = {
      nonce, chainId: 3, value: 0,
      to: contract_address,
      data: raw,
      gasPrice: web3.utils.toHex(10.1 * 1e9),
      gas: web3.utils.toHex(250000),
    }
    const signedTx = await account.signTransaction(tx);
    tx.time = new Date();
    web3.eth.sendSignedTransaction(signedTx.rawTransaction)
      .then(txn_hash => {
        res.ok({txn_hash});
      }).catch(err => {
        console.error(err);
        res.err(4, "Transaction error", err);
      }); // sendSignedTransaction
  }).catch(err => {
    res.err(5, 'Account error', err);
  }); // getAccounts
});

api.put('/toggle-pause', async (req, res) => {
  const web3 = new Web3(provider);
  const account = web3.eth.accounts.privateKeyToAccount(privkey);
  var tNonce = eval(web3.eth.getTransactionCount(account.address, 'pending'));
  var tTmp = eval(web3.eth.getTransactionCount(account.address));
  var [nonce, tmp] = [await tNonce, await tTmp];
  if (nonce !== tmp) return res.err(1, 'This account has pending transaction! Please wait.');
  const contract_address = token.getContractAddress();
  console.log('ContractAddress: ', contract_address);
  if (!contract_address) return res.err(2, 'Contract Address null', contract_address);
  var isPaused = await token.isPaused();
  var raw = isPaused? await token.buildUnpauseTx(): await token.buildPauseTx();
  const tx = {
    nonce, chainId: 3, value: 0,
    to: contract_address,
    data: raw,
    gasPrice: web3.utils.toHex(10.1 * 1e9),
    gas: web3.utils.toHex(250000),
  }
  const signedTx = await account.signTransaction(tx);
  tx.time = new Date();
  web3.eth.sendSignedTransaction(signedTx.rawTransaction)
    .then(async result => {
      var after = await token.isPaused()
      res.ok({isPaused: after});
    }).catch(err => {
      console.error(err);
      res.err(3, "Transaction error", err);
    }); // sendSignedTransaction
});

/**
 * Transfer ERC-20 Token
 */
api.post('/transfer', async (req, res) => {
  res.set('Content-Type', 'application/json');
  var address = req.body.address;
  var amount = parseInt(req.body.amount);
  if (!address || !amount) return res.err(1, 'Params Missing or of Wrong-type');

  // Perform Transfering
  const web3 = new Web3(provider);
  address = web3.utils.toChecksumAddress(address);
  web3.eth.getAccounts(async function(err, accounts) {
    const account = web3.eth.accounts.privateKeyToAccount(privkey);
    var tNonce = eval(web3.eth.getTransactionCount(account.address, 'pending'));
    var tTmp = eval(web3.eth.getTransactionCount(account.address));
    var [nonce, tmp] = [await tNonce, await tTmp];
    if (nonce !== tmp) return res.err(2, 'This account has pending transaction! Please wait.');
    const contract_address = token.getContractAddress();
    console.log('ContractAddress: ', contract_address);
    if (!contract_address) return res.err(3, 'Contract Address null', contract_address);
    const raw = await token.buildTransferTx(address, amount);
    const tx = {
      nonce, chainId: 3, value: 0,
      to: contract_address,
      data: raw,
      gasPrice: web3.utils.toHex(10.1 * 1e9),
      gas: web3.utils.toHex(250000),
    }
    const signedTx = await account.signTransaction(tx);
    tx.time = new Date();
    web3.eth.sendSignedTransaction(signedTx.rawTransaction)
      .then(txn_hash => {
        res.ok({txn_hash});
      }).catch(err => {
        console.log(err);
        res.err(4, "Transaction error", err);
      }); // sendSignedTransaction
  }).catch(err => {
    res.err(5, 'Account error', err);
  }); // getAccounts
});

module.exports = api;
