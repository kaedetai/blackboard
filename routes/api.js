// Create app
var api = require('../api')('/api');
var config = require('../config');
var AES = require('../utils/AES');
var Get = require('../utils/Get');
var Post = require('../utils/Post');
var Mongo = require('../utils/Mongo');
var Hash = require('../utils/Hash');
var QRCode = require('qrcode');
var moment = require('moment');

api.get('/merchants', async (req, res) => {
  res.ok(await Mongo.find('merchant'));
});

api.get('/transactions', {merchant: /.+/, _: {consumer: /.+/}}, async (req, res) => {
  let result = await Get.json('http://119.81.253.117:3001/api/v1/transactions');
  let txs = result.data.txs.map(x => x.data);
  txs = txs.filter(x => x.transaction.merchant == req.query.merchant);
  if (req.query.consumer)
    txs = txs.filter(x =>
      (x.consumer && x.consumer.id == req.query.consumer) ||
      (x.from && x.from.id == req.query.consumer) ||
      (x.to && x.to.id == req.query.consumer)
    );
  res.ok(txs);
});

api.get('/genPwd', {pwd: /.+/}, async (req, res) => {
  res.ok(Hash.md5(req.query.pwd));
});

api.post('/login', {account: /.+/, pwd: /.+/}, async (req, res) => {
  var now = moment().toISOString();
  var account = req.body.account;
  var pwd = req.body.pwd;

  var user = await Mongo.one('account', {account});
  if (!user) {
    await Mongo.add('login_log', {status: 0, account, hashPwd: Hash.md5(pwd), error: 'login error!', msg: 'account is not exists', date: now});
    return res.err(1, 'login error!', 'account is not exists');
  }
  if (Hash.md5(pwd) != user.pwd) {
    await Mongo.add('login_log', {status: 0, account, hashPwd: Hash.md5(pwd), error: 'login error!', msg: 'account is not exists', date: now});
    return res.err(2, 'login error!', 'worong password');
  }

  await Mongo.add('login_log', {status: 1, account, hashPwd: Hash.md5(pwd), error: '', msg: 'login success', date: now});
  delete user.pwd;
  return res.ok('login success');
});

api.unit('/account/1', {});
api.unit('/account/2', {});
api.get('/account/:id', async (req, res) => {
  var account = await Mongo.one('account', {account: req.query.id});
  if (!account) return res.err(1, 'The id is not exists');
  delete account.pwd;
  account.wallets = account.wallets.map(o => `resource:biz.fio.ConsumerMerchantWallet#user${account.id}-merchant${o}`);
  return res.ok({account});
});

api.post('/account', {
  account: /.+/, pwd: /.+/,
  _: {nickname: /.*/, gender: /.*/, birthday: /.*/, age: /.*/, region: /.*/}
}, async (req, res) => {
  var account = await Mongo.one('account', {account: req.body.account});
  if (account) return res.err(1, 'The account is exists');
  var data = {
    account: req.body.account,
    pwd: Hash.md5(req.body.pwd),
    tel: req.body.account,
    nickname: req.body.nickname || '',
    gender: req.body.gender || '',
    birthday: req.body.birthday || '',
    age: req.body.age || '',
    region: req.body.region || '',
  };
  await Mongo.add('account', data);
  delete data.pwd;
  return res.ok(data);
});

api.put('/account/:id', {
  _: {_pwd: /.+/, nickname: /.*/, gender: /.*/, birthday: /.*/, age: /.*/, region: /.*/}
}, async (req, res) => {
  var account = await Mongo.one('account', {account: req.params.account});
  if (!account) return res.err(1, 'The account is not exists');
  var data = {
    pwd: Hash.md5(req.body.pwd) || account.pwd,
    nickname: req.body.nickname || account.nickname,
    gender: req.body.gender || account.gender,
    birthday: req.body.birthday || account.birthday,
    age: req.body.age || account.age,
    region: req.body.region || account.region,
  };
  await Mongo.set('account', account._id, data);
  delete data.pwd;
  return res.ok(data);
});

api.post('/exchange/lct2fio', {account: /.+/, lct_name: /.+/, amount: /.+/}, async (req, res) => {
  var merchant = await Mongo.one('merchant', {name: req.body.lct_name});
  var user = await Mongo.one('account', {account: req.body.account});
  if (!user) return res.err(1, 'wrong account');
  if (!merchant) return res.err(2, 'wrong lct_name');
  let result = await Post.json('http://119.81.253.117:3000/api/ConsumerLCTToFioExchange', {
    "$class": "biz.fio.ConsumerLCTToFioExchange",
    "merchant": `resource:biz.fio.Merchant#merchant${merchant._id}`,
    "consumer": `resource:biz.fio.Consumer#user${user._id}`,
    "exechangRate": 1,
    "lctAmount": req.body.amount,
    "fioAmount": req.body.amount
  });
  result['class'] = result['$class'];
  delete result['$class'];
  await Mongo.add('exchange', {type: 'lct2fio', ... req.body, result});
  res.ok(result);
});

api.post('/exchange/fio2lct', {account: /.+/, lct_name: /.+/, amount: /.+/}, async (req, res) => {
  var merchant = await Mongo.one('merchant', {name: req.body.lct_name});
  var user = await Mongo.one('account', {account: req.body.account});
  if (!user) return res.err(1, 'wrong account');
  if (!merchant) return res.err(2, 'wrong lct_name');
  let result = await Post.json('http://119.81.253.117:3000/api/ConsumerFioToLCTExchange', {
    "$class": "biz.fio.ConsumerFioToLCTExchange",
    "merchant": `resource:biz.fio.Merchant#merchant${merchant._id}`,
    "consumer": `resource:biz.fio.Consumer#user${user._id}`,
    "exechangRate": 1,
    "lctAmount": req.body.amount,
    "fioAmount": req.body.amount
  });
  result['class'] = result['$class'];
  delete result['$class'];
  await Mongo.add('exchange', {type: 'fio2lct', ... req.body, result});
  res.ok(result);
});

api.post('/transfer/lct', {from_account_id: /.+/, to_account_id: /.+/, merchant_id: /.+/, amount: /.+/}, async (req, res) => {
  let result = await Post.json('http://119.81.253.117:3000/api/ConsumerLCTTransfering', {
    "$class": "biz.fio.ConsumerLCTTransfering",
    "consumerFrom": `resource:biz.fio.Consumer#user${req.body.from_account_id}`,
    "consumerTo": `resource:biz.fio.Consumer#user${req.body.to_account_id}`,
    "merchant": `resource:biz.fio.Merchant#merchant${req.body.merchant_id}`,
    "lctAmount": req.body.amount
  });
  result['class'] = result['$class'];
  delete result['$class'];
  await Mongo.add('transfer', {... req.body, result});
  res.ok(result);
});

api.get('/qrcode', async (req, res) => {
  var rs = await Mongo.find('qrcode', {}, {}, {_id: -1}, 30);
  var url = `${req.protocol}://${req.get('host')}`;
  for (k in rs) rs[k].url = `${url}/${rs[k].filename}`;
  return res.ok(rs);
});

api.post('/qrcode', {string: /.+/}, async (req, res) => {
  var t = moment().format("YYYYMMDD-hhmmss-SSS");
  var random = Math.floor(Math.random() * 1000);
  var filename = `${t}-${random}.png`;
  var filepath = `${config.images}/${filename}`;
  await Mongo.add('qrcode', {string: req.body.string, filename});
  QRCode.toFile(filepath, req.body.string, {
    color: {
      dark: '#00F',  // Blue dots
      light: '#0000' // Transparent background
    }
  }, function (err) {
    if (!err) return;
    console.log('Error! POST qrcode', err);
    return res.err(1, 'error', err);
  })
  var url = `${req.protocol}://${req.get('host')}/${filename}`;
  res.ok({string: req.body.string, url});
});

api.help('/help');
api.test('/test');

module.exports = api;
