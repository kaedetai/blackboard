// Create app
var api = require('../api')('/');
var config = require('../config');
var AES = require('../utils/AES');
var Get = require('../utils/Get');
var Post = require('../utils/Post');
var Mongo = require('../utils/Mongo');
var md5 = require('md5');

api.get('/init', async (req, res) => {
  await Mongo.insert('obj', [
    { _id: 1, type: 'rect', width: 40, height: 30, x: -30, y: -65, fill: '#c00' },
    { _id: 2, type: 'rect', width: 40, height: 30, x: 30, y: -65, fill: '#f80' },
    { _id: 3, type: 'rect', width: 40, height: 30, x: -30, y: 65, fill: '#09c' },
    { _id: 4, type: 'rect', width: 40, height: 30, x: 30, y: 65, fill: '#0c0' },
    { _id: 5, type: 'rect', width: 40, height: 30, x: 0, y: 0, fill: '#fff' },
    { _id: 6, type: 'text', x: -20, y: -30, text: '這是正常的文字', fill: '#00f' },
    { _id: 7, type: 'path', path: '#wave1', text: '這是隨著路徑跑的文字，很酷吧', fill: '#f00' },
    { _id: 8, type: 'path', path: '#wave2', text: '這是隨著路徑跑的文字，很酷吧', fill: '#792', dy: '50,10,20,5,40,8,20' },
  ]);
});

api.get('/', async (req, res) => {
  res.render({}, 'public/index.html');
});

api.get('/index.js', async (req, res) => {
  var objs = await Mongo.find('obj');
  res.data({objs}, 'public/index.js');
});

api.post('/obj', {obj: {}}, async (req, res) => {
  var obj = req.body.obj;
  var obj_id = await Mongo.add('obj', obj);
  var sio = require('../sio');
  sio.emit('obj', obj);
  res.ok(obj);
});

module.exports = api;
